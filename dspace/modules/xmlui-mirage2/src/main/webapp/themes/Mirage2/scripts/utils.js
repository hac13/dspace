/*
 * The contents of this file are subject to the license and copyright
 * detailed in the LICENSE and NOTICE files at the root of the source
 * tree and available online at
 *
 * http://www.dspace.org/license/
 */
(function($) {
    DSpace.getTemplate = function(name) {
        if (DSpace.dev_mode || DSpace.templates === undefined || DSpace.templates[name] === undefined) {
            $.ajax({
                url : DSpace.theme_path + 'templates/' + name + '.hbs',
                success : function(data) {
                    if (DSpace.templates === undefined) {
                        DSpace.templates = {};
                    }
                    DSpace.templates[name] = Handlebars.compile(data);
                },
                async : false
            });
        }
        return DSpace.templates[name];
    };

    /* ================== */
    /* DUL CUSTOMIZATIONS */
    /* ================== */

    $(function () {
      $('[data-toggle="tooltip"]').tooltip();
    });
    
    /* Put header above Altmetric badge only if that badge is displayed */
    /* See https://api.altmetric.com/embeds.html */
    $(function () {
        $('div.altmetric-embed').on('altmetric:show', function () {
            $( 'div.altmetric-embed[data-badge-type]' ).before( "<h5>Attention Stats</h5>" );
        });
    });

    /* Make the link to add items to a collection from a collection portal stand out */
    $(function () {
      $('#aspect_artifactbrowser_CollectionViewer_div_collection-view p.ds-paragraph a').addClass('btn btn-success');
    });


    /* Add a placeholder in the authority key value box on metadata edit view to */
    /* indicate it's for Duke Unique ID */
    $(function () {
      $('tr#aspect_administrative_item_EditItemMetadataForm_row_dc_contributor_author input.ds-authority-value').each(function(){
        $(this).attr('placeholder','Duke Unique ID');
      });
      $('tr#aspect_administrative_item_EditItemMetadataForm_row_dc_contributor_advisor input.ds-authority-value').each(function(){
        $(this).attr('placeholder','Duke Unique ID');
      });
    });

    /* Populate the embargo release date automatically when a months value is selected in web submission UI */

    $('#aspect_submission_StepTransformer_field_duke_embargo_months').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var embargoMonths = this.value;

        var now = new Date();
        var releaseDate = new Date();

        if ( embargoMonths == '0') {
          // Erase the release date
          $('#aspect_submission_StepTransformer_field_duke_embargo_release_year').val('');
          $('#aspect_submission_StepTransformer_field_duke_embargo_release_month option[value="0"]').prop("selected","selected");
          $('#aspect_submission_StepTransformer_field_duke_embargo_release_day').val('');
        } else if ( embargoMonths == '6') {
          releaseDate.setTime( now.getTime() + 183 * 86400000 ); // add 183 days
          $('#aspect_submission_StepTransformer_field_duke_embargo_release_year').val(releaseDate.getFullYear());
          $('#aspect_submission_StepTransformer_field_duke_embargo_release_month option[value=' + (releaseDate.getMonth() + 1) + ']').prop("selected", "selected");
          $('#aspect_submission_StepTransformer_field_duke_embargo_release_day').val(releaseDate.getDate());

        } else if ( embargoMonths == '12') {
          releaseDate.setFullYear(now.getFullYear() + 1);
          $('#aspect_submission_StepTransformer_field_duke_embargo_release_year').val(releaseDate.getFullYear());
          $('#aspect_submission_StepTransformer_field_duke_embargo_release_month option[value=' + (releaseDate.getMonth() + 1) + ']').prop("selected", "selected");
          $('#aspect_submission_StepTransformer_field_duke_embargo_release_day').val(releaseDate.getDate());

        } else if ( embargoMonths = '24') {
          releaseDate.setFullYear(now.getFullYear() + 2);
          $('#aspect_submission_StepTransformer_field_duke_embargo_release_year').val(releaseDate.getFullYear());
          $('#aspect_submission_StepTransformer_field_duke_embargo_release_month option[value=' + (releaseDate.getMonth() + 1) + ']').prop("selected", "selected");
          $('#aspect_submission_StepTransformer_field_duke_embargo_release_day').val(releaseDate.getDate());
        }

    });

    /* Don't let the embargo release year be more than two years in the future. */

    $( "#aspect_submission_StepTransformer_div_submit-describe" ).submit(function( event ) {
      var now = new Date();
      var maxReleaseYear = new Date();

      /* Adjust this if web submission forms should enable setting longer than two year embargo */
      maxReleaseYear.setFullYear(now.getFullYear() + 2);

      if ($('#aspect_submission_StepTransformer_field_duke_embargo_release_year').length > 0 && $('#aspect_submission_StepTransformer_field_duke_embargo_release_year').val().length > 0 && $('#aspect_submission_StepTransformer_field_duke_embargo_release_year').val() > maxReleaseYear.getFullYear()) {
        alert('Your embargo release date is too far in the future. Please contact lib-dspace-admin@duke.edu if you need or want an embargo for longer than two years.');
        event.preventDefault();
      } else {
        return;
      }
    });


})(jQuery);