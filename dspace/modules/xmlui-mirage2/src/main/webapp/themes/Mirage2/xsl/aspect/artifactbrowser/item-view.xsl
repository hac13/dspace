<!--

    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

-->

<!--
    Rendering specific to the item display page.

    Author: art.lowel at atmire.com
    Author: lieven.droogmans at atmire.com
    Author: ben at atmire.com
    Author: Alexey Maslov

-->

<xsl:stylesheet
    xmlns:i18n="http://apache.org/cocoon/i18n/2.1"
    xmlns:dri="http://di.tamu.edu/DRI/1.0/"
    xmlns:mets="http://www.loc.gov/METS/"
    xmlns:dim="http://www.dspace.org/xmlns/dspace/dim"
    xmlns:xlink="http://www.w3.org/TR/xlink/"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:atom="http://www.w3.org/2005/Atom"
    xmlns:ore="http://www.openarchives.org/ore/terms/"
    xmlns:oreatom="http://www.openarchives.org/ore/atom/"
    xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xalan="http://xml.apache.org/xalan"
    xmlns:encoder="xalan://java.net.URLEncoder"
    xmlns:util="org.dspace.app.xmlui.utils.XSLUtils"
    xmlns:jstring="java.lang.String"
    xmlns:rights="http://cosimo.stanford.edu/sdr/metsrights/"
    xmlns:confman="org.dspace.core.ConfigurationManager"
    exclude-result-prefixes="xalan encoder i18n dri mets dim xlink xsl util jstring rights confman">

    <xsl:output indent="yes"/>

    <xsl:template name="itemSummaryView-DIM">
        <!-- Generate the info about the item from the metadata section -->
        <xsl:apply-templates select="./mets:dmdSec/mets:mdWrap[@OTHERMDTYPE='DIM']/mets:xmlData/dim:dim"
        mode="itemSummaryView-DIM"/>

        <xsl:copy-of select="$SFXLink" />

        <!-- Generate the Creative Commons license information from the file section (DSpace deposit license hidden by default)
        <xsl:if test="./mets:fileSec/mets:fileGrp[@USE='CC-LICENSE' or @USE='LICENSE']">
            <div class="license-info table">
                <p>
                    <i18n:text>xmlui.dri2xhtml.METS-1.0.license-text</i18n:text>
                </p>
                <ul class="list-unstyled">
                    <xsl:apply-templates select="./mets:fileSec/mets:fileGrp[@USE='CC-LICENSE' or @USE='LICENSE']" mode="simple"/>
                </ul>
            </div>
        </xsl:if>
        -->
    </xsl:template>

    <!-- An item rendered in the detailView pattern, the "full item record" view of a DSpace item in Manakin. -->
    <xsl:template name="itemDetailView-DIM">
        <!-- Output all of the metadata about the item from the metadata section -->
        <xsl:apply-templates select="mets:dmdSec/mets:mdWrap[@OTHERMDTYPE='DIM']/mets:xmlData/dim:dim"
                             mode="itemDetailView-DIM"/>

        <!-- Generate the bitstream information from the file section -->
        <xsl:choose>
            <xsl:when test="./mets:fileSec/mets:fileGrp[@USE='CONTENT' or @USE='ORIGINAL' or @USE='LICENSE']/mets:file">
                <h3><i18n:text>xmlui.dri2xhtml.METS-1.0.item-files-head</i18n:text></h3>
                <div class="file-list">
                    <xsl:apply-templates select="./mets:fileSec/mets:fileGrp[@USE='CONTENT' or @USE='ORIGINAL' or @USE='LICENSE' or @USE='CC-LICENSE']">
                        <xsl:with-param name="context" select="."/>
                        <xsl:with-param name="primaryBitstream" select="./mets:structMap[@TYPE='LOGICAL']/mets:div[@TYPE='DSpace Item']/mets:fptr/@FILEID"/>
                    </xsl:apply-templates>
                </div>
            </xsl:when>
            <!-- Special case for handling ORE resource maps stored as DSpace bitstreams -->
            <xsl:when test="./mets:fileSec/mets:fileGrp[@USE='ORE']">
                <xsl:apply-templates select="./mets:fileSec/mets:fileGrp[@USE='ORE']" mode="itemDetailView-DIM" />
            </xsl:when>
            <xsl:otherwise>
                <h2><i18n:text>xmlui.dri2xhtml.METS-1.0.item-files-head</i18n:text></h2>
                <table class="ds-table file-list">
                    <tr class="ds-table-header-row">
                        <th><i18n:text>xmlui.dri2xhtml.METS-1.0.item-files-file</i18n:text></th>
                        <th><i18n:text>xmlui.dri2xhtml.METS-1.0.item-files-size</i18n:text></th>
                        <th><i18n:text>xmlui.dri2xhtml.METS-1.0.item-files-format</i18n:text></th>
                        <th><i18n:text>xmlui.dri2xhtml.METS-1.0.item-files-view</i18n:text></th>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <p><i18n:text>xmlui.dri2xhtml.METS-1.0.item-no-files</i18n:text></p>
                        </td>
                    </tr>
                </table>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>


    <xsl:template match="dim:dim" mode="itemSummaryView-DIM">
        <div class="item-summary-view-metadata">
            <xsl:call-template name="itemSummaryView-DIM-title"/>
            <div class="row">
                <div class="col-sm-4">
                    <div class="row" id="file-download-section">
                        <div class="col-xs-6 col-sm-12">
                            <xsl:call-template name="itemSummaryView-DIM-thumbnail"/>
                        </div>

                        <!-- DUL CUSTOMIZATION: show embargo release date -->
                        <xsl:choose>
                          <xsl:when test="dim:field[@mdschema='duke' and @element='embargo' and @qualifier='release']">
                            <div class="col-xs-6 col-sm-12 embargo-warning"><i class="fa fa-lock" aria-hidden="true"></i><br/>Access is limited until:<br/>
                              <strong>
                                <xsl:if test="count(dim:field[@mdschema='duke' and @element='embargo' and @qualifier='release']) &gt; 1">
                                <div class="spacer">&#160;</div>
                                </xsl:if>
                                <xsl:for-each select="dim:field[@mdschema='duke' and @element='embargo' and @qualifier='release']">
                                  <xsl:call-template name="format-date">
                                      <xsl:with-param name="DateTimeStr" select="./node()"/>
                                  </xsl:call-template>

                          <xsl:if test="count(following-sibling::dim:field[@mdschema='duke' and @element='embargo' and @qualifier='release']) != 0">
                                      <div class="spacer">&#160;</div>
                                      </xsl:if>
                                </xsl:for-each>
                                <xsl:if test="count(dim:field[@mdschema='duke' and @element='embargo' and @qualifier='release']) &gt; 1">
                                      <div class="spacer">&#160;</div>
                                </xsl:if>
                              </strong>
                            </div>
                          </xsl:when>
                        </xsl:choose>

                        <div class="col-xs-6 col-sm-12">
                            <xsl:call-template name="itemSummaryView-DIM-file-section"/>
                        </div>
                    </div>
                    <xsl:call-template name="itemSummaryView-DIM-date"/>
                    <xsl:call-template name="itemSummaryView-DIM-authors"/>

                    <!-- DUL CUSTOMIZATION: Add item pageviews -->
                    <!-- https://wiki.duraspace.org/display/DSPACE/Solr#Solr-QueryingSolrfromXMLUI -->

                    <!-- DUL CUSTOMIZATION: render page view stats -->

                    <h5><xsl:text>Repository Usage Stats</xsl:text></h5>

                    <!-- SCA: The OBJEDIT attribute from the METS document seems to be the only place
                      where the uuid for the item is available from these templates, and uuid is needed for
                      the Solr queries that can return the statistics. Revisit this later if it proves to be
                      too fragile or if there's a better way to get that ID. -->

                    <xsl:if test="/mets:METS/@OBJEDIT">
                      <xsl:variable name="item-stats-url">
                        <xsl:value-of select="$document//dri:options/dri:list[@id='aspect.statistics.Navigation.list.statistics']/dri:item/dri:xref/@target"/>
                      </xsl:variable>
                      <a class="stats-section-wrapper" href="{$item-stats-url}">
                        <div class="col-md-6 col-sm-6 stats-section-1">
                          <div class="stats-wrapper">
                            <span class="stats-count">
                              <xsl:call-template name="item-total-views">
                                <xsl:with-param name="item-id" select="substring-after(/mets:METS/@OBJEDIT,'itemID=')"/>
                              </xsl:call-template>
                            </span><br/>
                            <span class="stats-type">views</span>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 stats-section-2">
                          <div class="stats-wrapper">
                            <span class="stats-count">
                              <xsl:call-template name="item-total-downloads">
                                <xsl:with-param name="item-id" select="substring-after(/mets:METS/@OBJEDIT,'itemID=')"/>
                              </xsl:call-template>
                            </span><br/>
                            <span class="stats-type">downloads</span>
                          </div>
                        </div>
                      </a>
                    </xsl:if>

                    <!-- DUL CUSTOMIZATION: Add Altmetric badge; see https://api.altmetric.com/embeds.html -->
                    <!-- Badge markup will render DOI if present; if not, fall back and render handle -->
                    <!-- Badge can currently only accept one, so don't render both -->

                    <!-- Any way to know that this value is definitely a DOI? 10.xxxx.?-->
                    <xsl:choose>
                      <!-- Item has a DOI -->
                      <xsl:when test="dim:field[@element='relation' and @qualifier='isversionof']">
                        <xsl:variable name="isversionof-full-value">
                          <xsl:value-of select="dim:field[@element='relation' and @qualifier='isversionof'][1]/node()"/>
                        </xsl:variable>
                        <xsl:call-template name="altmetric-embed-badge">
                          <xsl:with-param name="identifier-type" select="'data-doi'"/>
                          <xsl:with-param name="identifier-value" select="$isversionof-full-value"/>
                        </xsl:call-template>
                      </xsl:when>

                      <!-- No DOI, fall back to Handle, then URI -->
                      <xsl:when test="dim:field[@element='identifier' and @qualifier='uri']">
                        <xsl:variable name="fullurl">
                          <xsl:value-of select="dim:field[@element='identifier' and @qualifier='uri'][1]/node()"/>
                        </xsl:variable>

                          <xsl:choose>
                            <!-- Find handle from URLs like http(s)://hdl.handle.net/10161/5070 -->
                            <xsl:when test="contains($fullurl, 'hdl.handle.net/')">
                              <xsl:call-template name="altmetric-embed-badge">
                                <xsl:with-param name="identifier-type" select="'data-handle'"/>
                                <xsl:with-param name="identifier-value" select="substring-after($fullurl, 'hdl.handle.net/')"/>
                              </xsl:call-template>
                            </xsl:when>
                            <!-- Find handle from URLs like {dspace app URL}/handle/10161/5070 -->
                            <xsl:when test="contains($fullurl, 'handle/')">
                              <xsl:call-template name="altmetric-embed-badge">
                                <xsl:with-param name="identifier-type" select="'data-handle'"/>
                                <xsl:with-param name="identifier-value" select="substring-after($fullurl, 'handle/')"/>
                              </xsl:call-template>
                            </xsl:when>

                            <xsl:otherwise>
                              <!-- No handle, just use URI for badge-->
                              <xsl:call-template name="altmetric-embed-badge">
                                <xsl:with-param name="identifier-type" select="'data-uri'"/>
                                <xsl:with-param name="identifier-value" select="$fullurl"/>
                              </xsl:call-template>
                            </xsl:otherwise>

                          </xsl:choose>

                      </xsl:when>
                    </xsl:choose>

                </div>
                <div class="col-sm-8">
                    <xsl:call-template name="itemSummaryView-DIM-abstract"/>
                    <!-- DUL CUSTOMIZATION: show department -->
                    <xsl:call-template name="itemSummaryView-DIM-department"/>
                    <!-- DUL CUSTOMIZATION: show description -->
                    <xsl:call-template name="itemSummaryView-DIM-description"/>
                    <!-- DUL CUSTOMIZATION: show type -->
                    <xsl:call-template name="itemSummaryView-DIM-type"/>
                    <!-- DUL CUSTOMIZATION: show subject -->
                    <xsl:call-template name="itemSummaryView-DIM-subject" />
                    <xsl:call-template name="itemSummaryView-DIM-URI"/>
                    <!-- DUL CUSTOMIZATION: link to published version via DOI -->
                    <xsl:call-template name="itemSummaryView-DIM-published-version"/>
                    <!-- DUL CUSTOMIZATION: show provenance -->
                    <xsl:call-template name="itemSummaryView-DIM-provenance"/>
                    <xsl:call-template name="itemSummaryView-collections"/>
                    <!-- DUL CUSTOMIZATION: move show full record from sidebar to bottom of main content -->
                    <xsl:if test="$ds_item_view_toggle_url != ''">
                      <xsl:call-template name="itemSummaryView-show-full"/>
                    </xsl:if>

                    <!-- DUL CUSTOMIZATION: pull in CC license into metadata summary section -->
                    <xsl:call-template name="cc-license-itempage">
                        <xsl:with-param name="metadataURL" select="$document//dri:referenceSet/dri:reference/@url"/>
                    </xsl:call-template>

                    <!-- DUL CUSTOMIZATION: get collection rights onto item view -->
                    <xsl:call-template name="itemSummaryView-collection-rights">
                      <xsl:with-param name="metadataURL" select="$document//dri:referenceSet/dri:reference[@type='DSpace Collection']/@url"/>
                    </xsl:call-template>

                </div>
            </div>
        </div>
    </xsl:template>

    <!-- DUL CUSTOMIZATION: render Altmetric badge with the applicable attributes for this item -->
    <xsl:template name="altmetric-embed-badge">
      <xsl:param name="identifier-type"/>
      <xsl:param name="identifier-value"/>

      <div class='altmetric-embed' data-badge-details='right' data-badge-type='medium-donut' data-hide-no-mentions='true'>
        <xsl:attribute name="{$identifier-type}">
          <xsl:value-of select="$identifier-value"/>
        </xsl:attribute>
      </div>

    </xsl:template>

    <xsl:template name="itemSummaryView-DIM-title">
        <xsl:choose>
            <xsl:when test="count(dim:field[@element='title'][not(@qualifier)]) &gt; 1">
                <h2 class="page-header first-page-header">
                    <xsl:value-of select="dim:field[@element='title'][not(@qualifier)][1]/node()"/>
                </h2>
                <div class="simple-item-view-other">
                    <p class="lead">
                        <xsl:for-each select="dim:field[@element='title'][not(@qualifier)]">
                            <xsl:if test="not(position() = 1)">
                                <xsl:value-of select="./node()"/>
                                <xsl:if test="count(following-sibling::dim:field[@element='title'][not(@qualifier)]) != 0">
                                    <xsl:text>; </xsl:text>
                                    <br/>
                                </xsl:if>
                            </xsl:if>

                        </xsl:for-each>
                    </p>
                </div>
            </xsl:when>
            <xsl:when test="count(dim:field[@element='title'][not(@qualifier)]) = 1">
                <h2 class="page-header first-page-header">
                    <xsl:value-of select="dim:field[@element='title'][not(@qualifier)][1]/node()"/>
                </h2>
            </xsl:when>
            <xsl:otherwise>
                <h2 class="page-header first-page-header">
                    <i18n:text>xmlui.dri2xhtml.METS-1.0.no-title</i18n:text>
                </h2>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="itemSummaryView-DIM-thumbnail">
      <!-- DUL CUSTOMIZATION: revise thumbnail rendering so it only appears if thumb is present/unrestricted. -->
      <!-- Also, link the image to the item (for single-file item) or file list (for multi-file item) -->
      <xsl:choose>
          <xsl:when test="//mets:fileSec/mets:fileGrp[@USE='THUMBNAIL']">
              <xsl:variable name="src">
                  <xsl:choose>
                      <xsl:when test="/mets:METS/mets:fileSec/mets:fileGrp[@USE='THUMBNAIL']/mets:file[@GROUPID=../../mets:fileGrp[@USE='CONTENT']/mets:file[@GROUPID=../../mets:fileGrp[@USE='THUMBNAIL']/mets:file/@GROUPID][1]/@GROUPID]">
                          <xsl:value-of
                                  select="/mets:METS/mets:fileSec/mets:fileGrp[@USE='THUMBNAIL']/mets:file[@GROUPID=../../mets:fileGrp[@USE='CONTENT']/mets:file[@GROUPID=../../mets:fileGrp[@USE='THUMBNAIL']/mets:file/@GROUPID][1]/@GROUPID]/mets:FLocat[@LOCTYPE='URL']/@xlink:href"/>
                      </xsl:when>
                      <xsl:otherwise>
                          <xsl:value-of
                                  select="//mets:fileSec/mets:fileGrp[@USE='THUMBNAIL']/mets:file/mets:FLocat[@LOCTYPE='URL']/@xlink:href"/>
                      </xsl:otherwise>
                  </xsl:choose>
              </xsl:variable>
              <!-- Checking if Thumbnail is restricted -->
              <xsl:choose>
                  <xsl:when test="contains($src,'isAllowed=n')"/>
                  <xsl:otherwise>
                    <a class="thumbnail">
                      <xsl:attribute name="href">
                        <xsl:choose>
                          <xsl:when test="count(//mets:fileSec/mets:fileGrp[@USE='CONTENT' or @USE='ORIGINAL' or @USE='LICENSE']/mets:file) = 1">
                            <xsl:value-of select="//mets:fileSec/mets:fileGrp[@USE='CONTENT' or @USE='ORIGINAL' or @USE='LICENSE']/mets:file[1]/mets:FLocat[@LOCTYPE='URL']/@xlink:href"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:text>#file-list</xsl:text>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:attribute>

                      <img class="img-thumbnail" alt="Thumbnail">
                          <xsl:attribute name="src">
                              <xsl:value-of select="$src"/>
                          </xsl:attribute>
                      </img>

                    </a>
                  </xsl:otherwise>
              </xsl:choose>
          </xsl:when>
          <xsl:otherwise>
            <!-- DUL CUSTOMIZATION: no image or placeholder -->
          </xsl:otherwise>
      </xsl:choose>
    </xsl:template>

    <xsl:template name="itemSummaryView-DIM-abstract">
        <xsl:if test="dim:field[@element='description' and @qualifier='abstract']">
            <div class="simple-item-view-description item-page-field-wrapper table">
                <h5 class="visible-xs"><i18n:text>xmlui.dri2xhtml.METS-1.0.item-abstract</i18n:text></h5>
                <div>
                    <xsl:for-each select="dim:field[@element='description' and @qualifier='abstract']">
                        <xsl:choose>
                            <xsl:when test="node()">
                              <!-- DUL CUSTOMIZATION: clean tags -->
                  						<xsl:call-template name="clean-html-tags">
                  							<xsl:with-param name="content" select="./text()"/>
                  						</xsl:call-template>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>&#160;</xsl:text>
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:if test="count(following-sibling::dim:field[@element='description' and @qualifier='abstract']) != 0">
                            <div class="spacer">&#160;</div>
                        </xsl:if>
                    </xsl:for-each>
                    <xsl:if test="count(dim:field[@element='description' and @qualifier='abstract']) &gt; 1">
                        <div class="spacer">&#160;</div>
                    </xsl:if>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="itemSummaryView-DIM-authors">
        <xsl:if test="dim:field[@element='contributor'][@qualifier='author' and descendant::text()] or dim:field[@element='creator' and descendant::text()] or dim:field[@element='contributor' and descendant::text()]">
            <div class="simple-item-view-authors item-page-field-wrapper table">
                <h5><i18n:text>xmlui.dri2xhtml.METS-1.0.item-author</i18n:text></h5>
                <xsl:choose>
                    <xsl:when test="dim:field[@element='contributor'][@qualifier='author']">
                        <xsl:for-each select="dim:field[@element='contributor'][@qualifier='author']">
                            <xsl:call-template name="itemSummaryView-DIM-authors-entry" />
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="dim:field[@element='creator']">
                        <xsl:for-each select="dim:field[@element='creator']">
                            <xsl:call-template name="itemSummaryView-DIM-authors-entry" />
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:when test="dim:field[@element='contributor']">
                        <xsl:for-each select="dim:field[@element='contributor']">
                            <xsl:call-template name="itemSummaryView-DIM-authors-entry" />
                        </xsl:for-each>
                    </xsl:when>
                    <xsl:otherwise>
                        <i18n:text>xmlui.dri2xhtml.METS-1.0.no-author</i18n:text>
                    </xsl:otherwise>
                </xsl:choose>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="itemSummaryView-DIM-authors-entry">
        <div>
            <xsl:if test="@authority">
                <xsl:attribute name="class"><xsl:text>ds-dc_contributor_author-authority</xsl:text></xsl:attribute>
            </xsl:if>
            <!-- DUL CUSTOMIZATION: custom author browse link to use authority if present -->
            <xsl:call-template name="AuthorBrowseLink">
              <xsl:with-param name="authorName" select="string(.)"/>
              <xsl:with-param name="authority" select="@authority"/>
              <xsl:with-param name="confidence" select="@confidence"/>
            </xsl:call-template>
        </div>
    </xsl:template>

    <!-- DUL CUSTOMIZATION: render author names with links to query by authority (duid), else string -->
    <xsl:template name="AuthorBrowseLink">
      <xsl:param name="authorName"/> <!--  must be a string -->
      <xsl:param name="authority"/>
      <xsl:param name="confidence"/>

      <a>
        <xsl:attribute name="href">
          <xsl:choose>
            <xsl:when test="string-length($authority) &gt; 0 and $authority != 'student' and $authority != 'inactive' and $confidence = 'ACCEPTED'">
              <xsl:value-of select="$context-path"/>
              <xsl:text>/browse?type=author&amp;authority=</xsl:text>
              <xsl:value-of select="$authority"/>
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$context-path"/>
              <xsl:text>/browse?type=author&amp;value=</xsl:text>
              <xsl:value-of select="encoder:encode($authorName)"/>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:attribute>
        <xsl:attribute name="title">
          <xsl:text>Browse by this author</xsl:text>
        </xsl:attribute>
        <xsl:value-of select="$authorName"/>
      </a>

      <!-- DUL CUSTOMIZATION: Link to an author's Scholars@Duke profile if present. -->
      <!-- Link will render only if the author has a DUID in DSpace (as authority key for -->
      <!-- dc.contributor.author) and a Scholars@Duke profile. -->
      <!-- Uses live data from the VIVO API; for future, consider a local data source, especially -->
      <!-- if there are any issues with request volume, uptime, or speed. -->
      <xsl:if test="string-length($authority) &gt; 0 and $authority != 'student' and $authority != 'inactive' and $confidence = 'ACCEPTED'">

        <!-- Construct a URL to retrieve data from Scholars@Duke via a SPARQL query -->
        <!-- The SPARQL query looks like this when not url-encoded:

        PREFIX duke:     <http://vivo.duke.edu/vivo/ontology/duke-extension#>
        SELECT *
        WHERE
        {
          ?person duke:duid ?duid; duke:profileAlias ?alias .
          FILTER (?duid = "0123456") .
        }
        LIMIT 20

        -->

        <xsl:variable name="vivo-sparql-query-url">
          <xsl:text>https://sparql.scholars.duke.edu/VIVO/query?api_key=</xsl:text>
          <xsl:value-of select="confman:getProperty('vivo.apiKey')" />
          <xsl:text>&amp;query=PREFIX%20duke%3A%20%20%20%20%20%3Chttp%3A%2F%2Fvivo.duke.edu%2Fvivo%2Fontology%2Fduke-extension%23%3E%0D%0ASELECT%20%2A%0D%0AWHERE%0D%0A%7B%0D%0A%20%20%3Fperson%20duke%3Aduid%20%3Fduid%3B%20duke%3AprofileAlias%20%3Falias%20.%0D%0A%20%20FILTER%20%28%3Fduid%20%3D%20%22</xsl:text>
          <xsl:value-of select="$authority" />
          <xsl:text>%22%29%20.%0D%0A%7D%0D%0ALIMIT%2020&amp;output=xml</xsl:text>
          <!-- <xsl:text>&amp;force-accept=text%2Fplain</xsl:text> -->
        </xsl:variable>

        <xsl:variable name="vivo-sparql-query-response" select="document($vivo-sparql-query-url)"/>

        <xsl:variable name="scholars-alias">
          <!-- NOTE: this XPath must use this complex local-name() convention because the XML -->
          <!-- returned in the Scholars@Duke SPARQL API response uses a default namespace but -->
          <!-- does not declare a prefix for it. -->
          <xsl:value-of select="$vivo-sparql-query-response/*[local-name()='sparql']/*[local-name()='results']/*[local-name()='result']/*[local-name()='binding'][@name='alias']/*[local-name()='literal']/text()" />
        </xsl:variable>

        <xsl:if test="string-length($scholars-alias) &gt; 0">
          <a class="scholars-icon-link" data-toggle="tooltip" data-placement="bottom" title="Scholars@Duke profile">
            <xsl:attribute name="href">
              <xsl:text>https://scholars.duke.edu/person/</xsl:text>
              <xsl:value-of select="$scholars-alias"/>
            </xsl:attribute>
          </a>
        </xsl:if>

      </xsl:if>

      <!-- DUL CUSTOMIZATION: show linked ORCID badge if person has an ORCID -->
      <!-- NOTE: name string has to be exact match in duke.contributor.orcid field -->
      <!-- which must structure values like this: Name|0000-0001-2345-6789 -->
      <xsl:for-each select="//dim:field[@mdschema='duke' and @element='contributor' and @qualifier='orcid']">
        <xsl:if test="substring-before(node(),'|') = $authorName">
          <a class="orcid-badge-link" data-toggle="tooltip" data-placement="bottom">
            <xsl:attribute name="href">
              <xsl:text>https://orcid.org/</xsl:text>
              <xsl:value-of select="substring-after(node(),'|')"/>
            </xsl:attribute>
            <xsl:attribute name="title">
              <xsl:text>ORCID profile</xsl:text>
            </xsl:attribute>
          </a>
        </xsl:if>
      </xsl:for-each>

    </xsl:template>

    <xsl:template name="itemSummaryView-DIM-URI">
        <xsl:if test="dim:field[@element='identifier' and @qualifier='uri' and descendant::text()]">
            <div class="simple-item-view-uri item-page-field-wrapper table">
                <h5>Permalink</h5>
                <span>
                    <xsl:for-each select="dim:field[@element='identifier' and @qualifier='uri']">
                        <a>
                            <xsl:attribute name="href">
                                <xsl:copy-of select="./node()"/>
                            </xsl:attribute>
                            <xsl:copy-of select="./node()"/>
                        </a>
                        <xsl:if test="count(following-sibling::dim:field[@element='identifier' and @qualifier='uri']) != 0">
                            <br/>
                        </xsl:if>
                    </xsl:for-each>
                </span>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="itemSummaryView-DIM-date">
        <xsl:if test="dim:field[@element='date' and @qualifier='issued' and descendant::text()]">
            <div class="simple-item-view-date word-break item-page-field-wrapper table">
                <h5>
                    <i18n:text>xmlui.dri2xhtml.METS-1.0.item-date</i18n:text>
                </h5>
                <xsl:for-each select="dim:field[@element='date' and @qualifier='issued']">
                    <xsl:copy-of select="substring(./node(),1,10)"/>
                    <xsl:if test="count(following-sibling::dim:field[@element='date' and @qualifier='issued']) != 0">
                        <br/>
                    </xsl:if>
                </xsl:for-each>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="itemSummaryView-show-full">
        <div class="simple-item-view-show-full item-page-field-wrapper table">
            <h5>
                <i18n:text>xmlui.mirage2.itemSummaryView.MetaData</i18n:text>
            </h5>
            <a>
                <xsl:attribute name="href"><xsl:value-of select="$ds_item_view_toggle_url"/></xsl:attribute>
                <i18n:text>xmlui.ArtifactBrowser.ItemViewer.show_full</i18n:text>
            </a>
        </div>
    </xsl:template>


    <!-- DUL CUSTOMIZATION: Add department to brief metadata view -->
    <xsl:template name="itemSummaryView-DIM-department">
        <xsl:if test="dim:field[@element='department']">
          <div class="simple-item-view-department item-page-field-wrapper table">
            <h5>Department</h5>
            <xsl:for-each select="dim:field[@element='department']">
              <xsl:value-of select="node()"/>
              <xsl:if test="count(following-sibling::dim:field[@element='department']) != 0"> <br/> </xsl:if>
            </xsl:for-each>
          </div>
        </xsl:if>
    </xsl:template>

    <!-- DUL CUSTOMIZATION: Add description to brief metadata view -->
    <xsl:template name="itemSummaryView-DIM-description">
        <xsl:if test="dim:field[@element='description'][not(@qualifier)]">
          <div class="simple-item-view-description item-page-field-wrapper table">
            <h5>Description</h5>
            <xsl:for-each select="dim:field[@element='description'][not(@qualifier)]">
              <xsl:value-of select="node()"/>
              <xsl:if test="count(following-sibling::dim:field[@element='description'][not(@qualifier)]) != 0"> <br/> </xsl:if>
            </xsl:for-each>
          </div>
        </xsl:if>
    </xsl:template>

    <!-- DUL CUSTOMIZATION: Add type to brief metadata view -->
    <xsl:template name="itemSummaryView-DIM-type">
        <xsl:if test="dim:field[@element='type'][not(@qualifier)]">
          <div class="simple-item-view-type item-page-field-wrapper table">
            <h5>Type</h5>
            <xsl:for-each select="dim:field[@element='type'][not(@qualifier)]">
              <xsl:value-of select="node()"/>
              <xsl:if test="count(following-sibling::dim:field[@element='type'][not(@qualifier)]) != 0"> <br/> </xsl:if>
            </xsl:for-each>
          </div>
        </xsl:if>
    </xsl:template>

    <!-- DUL CUSTOMIZATION: Add subject to brief metadata view -->
    <xsl:template name="itemSummaryView-DIM-subject">
        <xsl:if test="dim:field[@element='subject']">
          <div class="simple-item-view-subject item-page-field-wrapper table">
            <h5>Subject</h5>
            <xsl:for-each select="dim:field[@element='subject']">
                <a href="{$context-path}/browse?type=subject&amp;value={node()}"><xsl:value-of select="node()"/></a>
                <xsl:if test="count(following-sibling::dim:field[@element='subject']) != 0"> <br /> </xsl:if>
            </xsl:for-each>
          </div>
        </xsl:if>
    </xsl:template>

    <!-- DUL CUSTOMIZATION: Add published version link to brief metadata view -->
    <!-- NOTE: Revisit implementation if we need to store anything other than DOIs in this field -->
    <!-- or make the DOI resolver base URL configurable. -->
    <xsl:template name="itemSummaryView-DIM-published-version">
        <xsl:if test="dim:field[@element='relation' and @qualifier='isversionof' and descendant::text()]">
          <div class="simple-item-view-published-version item-page-field-wrapper table">
            <h5>Published Version <small>(Please cite this version)</small></h5>
            <xsl:for-each select="dim:field[@element='relation' and @qualifier='isversionof']">
              <xsl:variable name="versionofvalue" select="node()"/>
              <xsl:choose>
                <!-- If it's a URL, just present it as a link -->
                <xsl:when test="starts-with($versionofvalue, 'http')">
                  <a href="{$versionofvalue}">
                    <xsl:value-of select="$versionofvalue"/>
                  </a>
                </xsl:when>
                <!-- If not, see if it's a DOI & link to that using hard-coded resolver. -->
                <xsl:when test="starts-with($versionofvalue, '10.') or starts-with($versionofvalue, 'doi')">
                  <!-- Remove spaces, esp for values like "doi: 10.xxxx" else the links won't work -->
                  <xsl:variable name="versionofvalue-stripped" select="translate($versionofvalue,' ','')" />
                  <a href="https://dx.doi.org/{$versionofvalue-stripped}">
                    <xsl:value-of select="$versionofvalue"/>
                  </a>
                </xsl:when>
                <!-- Otherwise, just list the value without linking -->
                <xsl:otherwise>
                  <xsl:value-of select="$versionofvalue"/>
                </xsl:otherwise>
              </xsl:choose>
              <xsl:if test="count(following-sibling::dim:field[@element='relation' and @qualifier='isversionof']) != 0"> <br /> </xsl:if>
            </xsl:for-each>
          </div>
        </xsl:if>
    </xsl:template>


    <!-- DUL CUSTOMIZATION: Add public provenance note (dcterms.provenance) to brief metadata view -->
    <xsl:template name="itemSummaryView-DIM-provenance">
        <xsl:if test="dim:field[@element='provenance' and descendant::text()][not(@qualifier)]">
          <div class="simple-item-view-provenance item-page-field-wrapper table">
            <h5>Provenance</h5>
            <xsl:for-each select="dim:field[@element='provenance'][not(@qualifier)]">
              <xsl:value-of select="node()"/>
              <xsl:if test="count(following-sibling::dim:field[@element='provenance'][not(@qualifier)]) != 0"> <br/> </xsl:if>
            </xsl:for-each>
          </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="itemSummaryView-collections">
        <xsl:if test="$document//dri:referenceSet[@id='aspect.artifactbrowser.ItemViewer.referenceSet.collection-viewer']">
            <div class="simple-item-view-collections item-page-field-wrapper table">
                <h5>
                    <i18n:text>xmlui.mirage2.itemSummaryView.Collections</i18n:text>
                </h5>
                <xsl:apply-templates select="$document//dri:referenceSet[@id='aspect.artifactbrowser.ItemViewer.referenceSet.collection-viewer']/dri:reference"/>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="itemSummaryView-DIM-file-section">
        <xsl:choose>
            <xsl:when test="//mets:fileSec/mets:fileGrp[@USE='CONTENT' or @USE='ORIGINAL' or @USE='LICENSE']/mets:file">
                <div class="item-page-field-wrapper table word-break">
                    <!-- DUL CUSTOMIZATION: don't show header for View/Open -->
                    <!-- 
                    <h5>
                        <i18n:text>xmlui.dri2xhtml.METS-1.0.item-files-viewOpen</i18n:text>
                    </h5>
                    -->

                    <!-- DUL CUSTOMIZATION: count files to accommodate custom display for single vs. multi-file -->
                    <xsl:variable name="total-file-count">
                      <xsl:value-of select="count(//mets:fileSec/mets:fileGrp[@USE='CONTENT' or @USE='ORIGINAL' or @USE='LICENSE']/mets:file)"/>
                    </xsl:variable>

                    <xsl:variable name="label-1">
                            <xsl:choose>
                                <xsl:when test="confman:getProperty('mirage2.item-view.bitstream.href.label.1')">
                                    <xsl:value-of select="confman:getProperty('mirage2.item-view.bitstream.href.label.1')"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>label</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                    </xsl:variable>

                    <xsl:variable name="label-2">
                            <xsl:choose>
                                <xsl:when test="confman:getProperty('mirage2.item-view.bitstream.href.label.2')">
                                    <xsl:value-of select="confman:getProperty('mirage2.item-view.bitstream.href.label.2')"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>title</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                    </xsl:variable>

                    <!-- DUL CUSTOMIZATION: show file header only if multi-file -->
                    <xsl:if test="$total-file-count &gt; 1">
                      <h5 id="file-list">Files</h5>
                    </xsl:if>

                    <xsl:for-each select="//mets:fileSec/mets:fileGrp[@USE='CONTENT' or @USE='ORIGINAL' or @USE='LICENSE']/mets:file">
                        <xsl:call-template name="itemSummaryView-DIM-file-section-entry">
                            <xsl:with-param name="href" select="mets:FLocat[@LOCTYPE='URL']/@xlink:href" />
                            <xsl:with-param name="mimetype" select="@MIMETYPE" />
                            <xsl:with-param name="label-1" select="$label-1" />
                            <xsl:with-param name="label-2" select="$label-2" />
                            <xsl:with-param name="title" select="mets:FLocat[@LOCTYPE='URL']/@xlink:title" />
                            <xsl:with-param name="label" select="mets:FLocat[@LOCTYPE='URL']/@xlink:label" />
                            <xsl:with-param name="total-file-count" select="$total-file-count" />
                            <xsl:with-param name="size" select="@SIZE" />
                        </xsl:call-template>
                    </xsl:for-each>
                </div>
            </xsl:when>
            <!-- Special case for handling ORE resource maps stored as DSpace bitstreams -->
            <xsl:when test="//mets:fileSec/mets:fileGrp[@USE='ORE']">
                <xsl:apply-templates select="//mets:fileSec/mets:fileGrp[@USE='ORE']" mode="itemSummaryView-DIM" />
            </xsl:when>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="itemSummaryView-DIM-file-section-entry">
        <xsl:param name="href" />
        <xsl:param name="mimetype" />
        <xsl:param name="label-1" />
        <xsl:param name="label-2" />
        <xsl:param name="title" />
        <xsl:param name="label" />
        <xsl:param name="total-file-count" />
        <xsl:param name="size" />
        <div>
            <!-- DUL CUSTOMIZATION: Render file links with custom icons -->
            <a class="file-with-icon" data-mimetype="{$mimetype}">
                <xsl:attribute name="href">
                    <xsl:value-of select="$href"/>
                </xsl:attribute>

                <!-- DUL CUSTOMIZATION: single-file items show filename as tooltip -->
                <xsl:if test="$total-file-count = 1">
                  <xsl:attribute name="data-toggle">tooltip</xsl:attribute>
                  <xsl:attribute name="data-placement">bottom</xsl:attribute>
                  <xsl:attribute name="title">
                    <xsl:call-template name="file-label-or-title">
                      <xsl:with-param name="mimetype" select="$mimetype" />
                      <xsl:with-param name="label-1" select="$label-1" />
                      <xsl:with-param name="label-2" select="$label-2" />
                      <xsl:with-param name="title" select="$title" />
                      <xsl:with-param name="label" select="$label" />
                    </xsl:call-template>
                  </xsl:attribute>
                </xsl:if>


                <xsl:call-template name="getFileIcon">
                    <xsl:with-param name="mimetype">
                        <xsl:value-of select="substring-before($mimetype,'/')"/>
                        <xsl:text>/</xsl:text>
                        <xsl:value-of select="substring-after($mimetype,'/')"/>
                    </xsl:with-param>
                </xsl:call-template>

                <!-- DUL CUSTOMIZATION: Single vs. Multi-File Items. Simplify Single-File -->
                <xsl:choose>
                  <xsl:when test="$total-file-count = 1">
                    <xsl:text>View / Download</xsl:text>
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:call-template name="file-label-or-title">
                      <xsl:with-param name="mimetype" select="$mimetype" />
                      <xsl:with-param name="label-1" select="$label-1" />
                      <xsl:with-param name="label-2" select="$label-2" />
                      <xsl:with-param name="title" select="$title" />
                      <xsl:with-param name="label" select="$label" />
                    </xsl:call-template>
                  </xsl:otherwise>
                </xsl:choose>

                <!-- DUL CUSTOMIZATION: tweak filesize display; use format-number to show one decimal value -->
                <br/>
                <span class="file-size">
                  <xsl:choose>
                      <xsl:when test="$size &lt; 1024">
                          <xsl:value-of select="format-number($size, '#.0')" />
                          <xsl:text> </xsl:text>
                          <i18n:text>xmlui.dri2xhtml.METS-1.0.size-bytes</i18n:text>
                      </xsl:when>
                      <xsl:when test="$size &lt; 1024 * 1024">
                          <xsl:value-of select="format-number(($size div 1024), '#.0')" />
                          <xsl:text> </xsl:text>
                          <i18n:text>xmlui.dri2xhtml.METS-1.0.size-kilobytes</i18n:text>
                      </xsl:when>
                      <xsl:when test="$size &lt; 1024 * 1024 * 1024">
                          <xsl:value-of select="format-number(($size div (1024 * 1024)), '#.0')" />
                          <xsl:text> </xsl:text>
                          <i18n:text>xmlui.dri2xhtml.METS-1.0.size-megabytes</i18n:text>
                      </xsl:when>
                      <xsl:otherwise>
                          <xsl:value-of select="format-number(($size div (1024 * 1024 * 1024)), '#.0')" />
                          <xsl:text> </xsl:text>
                          <i18n:text>xmlui.dri2xhtml.METS-1.0.size-gigabytes</i18n:text>
                      </xsl:otherwise>
                  </xsl:choose>
                </span>
            </a>
        </div>
    </xsl:template>

    <!-- DUL CUSTOMIZATION: Display the parent collection's rights text if present -->
    <xsl:template name="itemSummaryView-collection-rights">
        <xsl:param name="metadataURL"/>
        <xsl:variable name="externalMetadataURL">
            <xsl:text>cocoon:/</xsl:text>
            <xsl:value-of select="$metadataURL"/>
        </xsl:variable>

        <xsl:if test="string-length(document($externalMetadataURL)//dim:field[@element='rights'][not(@qualifier)])&gt;0">
          <div class="cc-license-section">
            <p>
              <xsl:copy-of select="document($externalMetadataURL)//dim:field[@element='rights'][not(@qualifier)]/node()"/>
            </p>
            <p class="small text-muted">
              <xsl:text>Rights for Collection: </xsl:text>
              <a>
                <xsl:attribute name="href">
                  <xsl:value-of select="document($externalMetadataURL)/mets:METS/@OBJID"/>
                </xsl:attribute>
                <xsl:value-of select="document($externalMetadataURL)//dim:field[@element='title'][1]"/>
              </a>
            </p>
          </div>
        </xsl:if>

    </xsl:template>

    <!-- DUL CUSTOMIZATION: Display item views on item page by parsing Solr results -->
    <xsl:template name="item-total-views">
      <xsl:param name="item-id" />
      <xsl:variable name="solr-statistics-url" select="confman:getProperty('solr-statistics', 'server')"/>
      <xsl:apply-templates select="document(concat($solr-statistics-url, '/select?indent=on&amp;q=type:+2+AND++%28id:',$item-id,'+OR+id:null%29&amp;facet.limit=1&amp;facet.field=id&amp;facet.mincount=1&amp;fq=-isBot:true&amp;fq=-%28bundleName:%5B*+TO+*%5D-bundleName:ORIGINAL%29&amp;fq=-%28statistics_type:%5B*+TO+*%5D+-statistics_type:view%29&amp;rows=0&amp;facet=true&amp;omitHeader=true'))" mode="itemStatsViews"/>
      <xsl:comment><xsl:value-of select="$item-id"/></xsl:comment>
    </xsl:template>

    <!-- DUL CUSTOMIZATION: get item views from Solr to display on item page -->
    <xsl:template match="/response" mode="itemStatsViews">
      <xsl:variable name="item-views-integer" select="result[@name='response']/@numFound"/>
      <xsl:value-of select='format-number($item-views-integer, "###,###")'/>
    </xsl:template>

    <!-- DUL CUSTOMIZATION: Display item downloads on item page by parsing Solr results -->
    <xsl:template name="item-total-downloads">
      <xsl:param name="item-id" />
      <xsl:variable name="solr-statistics-url" select="confman:getProperty('solr-statistics', 'server')"/>
      <xsl:apply-templates select="document(concat($solr-statistics-url, '/select?indent=on&amp;q=type:+0+AND+%28owningItem:',$item-id,'+OR+owningItem:null%29&amp;facet.limit=1&amp;facet.field=id&amp;facet.mincount=1&amp;fq=-isBot:true&amp;fq=-%28bundleName:%5B*+TO+*%5D-bundleName:ORIGINAL%29&amp;fq=-%28statistics_type:%5B*+TO+*%5D+AND+-statistics_type:view%29&amp;rows=0&amp;facet=true&amp;omitHeader=true'))" mode="itemStatsViews"/>
    </xsl:template>

    <!-- DUL CUSTOMIZATION: get file downloads from Solr to display on item page -->
    <xsl:template match="/response" mode="itemStatsDownloads">
      <xsl:variable name="item-downloads-integer" select="result[@name='response']/@numFound"/>
      <xsl:value-of select='format-number($item-downloads-integer, "###,###")'/>
    </xsl:template>

    <!-- DUL CUSTOMIZATION: Moved this logic to own named template for reuse -->
    <xsl:template name="file-label-or-title">
      <xsl:param name="mimetype" />
      <xsl:param name="label-1" />
      <xsl:param name="label-2" />
      <xsl:param name="title" />
      <xsl:param name="label" />
      <xsl:choose>
          <xsl:when test="contains($label-1, 'label') and string-length($label)!=0">
              <xsl:value-of select="$label"/>
          </xsl:when>
          <xsl:when test="contains($label-1, 'title') and string-length($title)!=0">
              <xsl:value-of select="$title"/>
          </xsl:when>
          <xsl:when test="contains($label-2, 'label') and string-length($label)!=0">
              <xsl:value-of select="$label"/>
          </xsl:when>
          <xsl:when test="contains($label-2, 'title') and string-length($title)!=0">
              <xsl:value-of select="$title"/>
          </xsl:when>
          <xsl:otherwise>
              <xsl:call-template name="getFileTypeDesc">
                  <xsl:with-param name="mimetype">
                      <xsl:value-of select="substring-before($mimetype,'/')"/>
                      <xsl:text>/</xsl:text>
                      <xsl:choose>
                          <xsl:when test="contains($mimetype,';')">
                              <xsl:value-of select="substring-before(substring-after($mimetype,'/'),';')"/>
                          </xsl:when>
                          <xsl:otherwise>
                              <xsl:value-of select="substring-after($mimetype,'/')"/>
                          </xsl:otherwise>
                      </xsl:choose>
                  </xsl:with-param>
              </xsl:call-template>
          </xsl:otherwise>
      </xsl:choose>
    </xsl:template>

    <xsl:template match="dim:dim" mode="itemDetailView-DIM">
        <xsl:call-template name="itemSummaryView-DIM-title"/>
        <div class="ds-table-responsive">
            <table class="ds-includeSet-table detailtable table table-striped table-hover">
                <xsl:apply-templates mode="itemDetailView-DIM"/>
            </table>
        </div>

        <span class="Z3988">
            <xsl:attribute name="title">
                 <xsl:call-template name="renderCOinS"/>
            </xsl:attribute>
            &#xFEFF; <!-- non-breaking space to force separating the end tag -->
        </span>
        <xsl:copy-of select="$SFXLink" />
    </xsl:template>

    <xsl:template match="dim:field" mode="itemDetailView-DIM">
            <tr>
                <xsl:attribute name="class">
                    <xsl:text>ds-table-row </xsl:text>
                    <xsl:if test="(position() div 2 mod 2 = 0)">even </xsl:if>
                    <xsl:if test="(position() div 2 mod 2 = 1)">odd </xsl:if>
                </xsl:attribute>
                <td class="label-cell">
                    <xsl:value-of select="./@mdschema"/>
                    <xsl:text>.</xsl:text>
                    <xsl:value-of select="./@element"/>
                    <xsl:if test="./@qualifier">
                        <xsl:text>.</xsl:text>
                        <xsl:value-of select="./@qualifier"/>
                    </xsl:if>
                </td>
            <td class="word-break">
              <xsl:copy-of select="./node()"/>
            </td>
                <td><xsl:value-of select="./@language"/></td>
            </tr>
    </xsl:template>

    <!-- don't render the item-view-toggle automatically in the summary view, only when it gets called -->
    <xsl:template match="dri:p[contains(@rend , 'item-view-toggle') and
        (preceding-sibling::dri:referenceSet[@type = 'summaryView'] or following-sibling::dri:referenceSet[@type = 'summaryView'])]">
    </xsl:template>

    <!-- don't render the head on the item view page -->
    <xsl:template match="dri:div[@n='item-view']/dri:head" priority="5">
    </xsl:template>

    <xsl:template match="mets:fileGrp[@USE='CONTENT']">
        <xsl:param name="context"/>
        <xsl:param name="primaryBitstream" select="-1"/>
            <xsl:choose>
                <!-- If one exists and it's of text/html MIME type, only display the primary bitstream -->
                <xsl:when test="mets:file[@ID=$primaryBitstream]/@MIMETYPE='text/html'">
                    <xsl:apply-templates select="mets:file[@ID=$primaryBitstream]">
                        <xsl:with-param name="context" select="$context"/>
                    </xsl:apply-templates>
                </xsl:when>
                <!-- Otherwise, iterate over and display all of them -->
                <xsl:otherwise>
                    <xsl:apply-templates select="mets:file">
                     	<!--Do not sort any more bitstream order can be changed-->
                        <xsl:with-param name="context" select="$context"/>
                    </xsl:apply-templates>
                </xsl:otherwise>
            </xsl:choose>
    </xsl:template>

    <xsl:template match="mets:fileGrp[@USE='LICENSE']">
        <xsl:param name="context"/>
        <xsl:param name="primaryBitstream" select="-1"/>
            <xsl:apply-templates select="mets:file">
                        <xsl:with-param name="context" select="$context"/>
            </xsl:apply-templates>
    </xsl:template>

    <xsl:template match="mets:file">
        <xsl:param name="context" select="."/>
        <div class="file-wrapper row">
            <div class="col-xs-6 col-sm-3">
                <div class="thumbnail">
                    <a class="image-link">
                        <xsl:attribute name="href">
                            <xsl:value-of select="mets:FLocat[@LOCTYPE='URL']/@xlink:href"/>
                        </xsl:attribute>
                        <xsl:choose>
                            <xsl:when test="$context/mets:fileSec/mets:fileGrp[@USE='THUMBNAIL']/
                        mets:file[@GROUPID=current()/@GROUPID]">
                                <img class="img-thumbnail" alt="Thumbnail">
                                    <xsl:attribute name="src">
                                        <xsl:value-of select="$context/mets:fileSec/mets:fileGrp[@USE='THUMBNAIL']/
                                    mets:file[@GROUPID=current()/@GROUPID]/mets:FLocat[@LOCTYPE='URL']/@xlink:href"/>
                                    </xsl:attribute>
                                </img>
                            </xsl:when>
                            <xsl:otherwise>
                                <img class="img-thumbnail" alt="Thumbnail">
                                    <xsl:attribute name="data-src">
                                        <xsl:text>holder.js/100%x</xsl:text>
                                        <xsl:value-of select="$thumbnail.maxheight"/>
                                        <xsl:text>/text:No Thumbnail</xsl:text>
                                    </xsl:attribute>
                                </img>
                            </xsl:otherwise>
                        </xsl:choose>
                    </a>
                </div>
            </div>

            <div class="col-xs-6 col-sm-7">
                <dl class="file-metadata dl-horizontal">
                    <dt>
                        <i18n:text>xmlui.dri2xhtml.METS-1.0.item-files-name</i18n:text>
                        <xsl:text>:</xsl:text>
                    </dt>
                    <dd class="word-break">
                        <xsl:attribute name="title">
                            <xsl:value-of select="mets:FLocat[@LOCTYPE='URL']/@xlink:title"/>
                        </xsl:attribute>
                        <xsl:value-of select="util:shortenString(mets:FLocat[@LOCTYPE='URL']/@xlink:title, 30, 5)"/>
                    </dd>
                <!-- File size always comes in bytes and thus needs conversion -->
                    <dt>
                        <i18n:text>xmlui.dri2xhtml.METS-1.0.item-files-size</i18n:text>
                        <xsl:text>:</xsl:text>
                    </dt>
                    <dd class="word-break">
                        <xsl:choose>
                            <xsl:when test="@SIZE &lt; 1024">
                                <xsl:value-of select="@SIZE"/>
                                <i18n:text>xmlui.dri2xhtml.METS-1.0.size-bytes</i18n:text>
                            </xsl:when>
                            <xsl:when test="@SIZE &lt; 1024 * 1024">
                                <xsl:value-of select="substring(string(@SIZE div 1024),1,5)"/>
                                <i18n:text>xmlui.dri2xhtml.METS-1.0.size-kilobytes</i18n:text>
                            </xsl:when>
                            <xsl:when test="@SIZE &lt; 1024 * 1024 * 1024">
                                <xsl:value-of select="substring(string(@SIZE div (1024 * 1024)),1,5)"/>
                                <i18n:text>xmlui.dri2xhtml.METS-1.0.size-megabytes</i18n:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="substring(string(@SIZE div (1024 * 1024 * 1024)),1,5)"/>
                                <i18n:text>xmlui.dri2xhtml.METS-1.0.size-gigabytes</i18n:text>
                            </xsl:otherwise>
                        </xsl:choose>
                    </dd>
                <!-- Lookup File Type description in local messages.xml based on MIME Type.
         In the original DSpace, this would get resolved to an application via
         the Bitstream Registry, but we are constrained by the capabilities of METS
         and can't really pass that info through. -->
                    <dt>
                        <i18n:text>xmlui.dri2xhtml.METS-1.0.item-files-format</i18n:text>
                        <xsl:text>:</xsl:text>
                    </dt>
                    <dd class="word-break">
                        <xsl:call-template name="getFileTypeDesc">
                            <xsl:with-param name="mimetype">
                                <xsl:value-of select="substring-before(@MIMETYPE,'/')"/>
                                <xsl:text>/</xsl:text>
                                <xsl:choose>
                                    <xsl:when test="contains(@MIMETYPE,';')">
                                <xsl:value-of select="substring-before(substring-after(@MIMETYPE,'/'),';')"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="substring-after(@MIMETYPE,'/')"/>
                                    </xsl:otherwise>
                                </xsl:choose>

                            </xsl:with-param>
                        </xsl:call-template>
                    </dd>
                <!-- Display the contents of 'Description' only if bitstream contains a description -->
                <xsl:if test="mets:FLocat[@LOCTYPE='URL']/@xlink:label != ''">
                        <dt>
                            <i18n:text>xmlui.dri2xhtml.METS-1.0.item-files-description</i18n:text>
                            <xsl:text>:</xsl:text>
                        </dt>
                        <dd class="word-break">
                            <xsl:attribute name="title">
                                <xsl:value-of select="mets:FLocat[@LOCTYPE='URL']/@xlink:label"/>
                            </xsl:attribute>
                            <xsl:value-of select="util:shortenString(mets:FLocat[@LOCTYPE='URL']/@xlink:label, 30, 5)"/>
                        </dd>
                </xsl:if>
                </dl>
            </div>

            <div class="file-link col-xs-6 col-xs-offset-6 col-sm-2 col-sm-offset-0">
                <xsl:choose>
                    <xsl:when test="@ADMID">
                        <xsl:call-template name="display-rights"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:call-template name="view-open"/>
                    </xsl:otherwise>
                </xsl:choose>
            </div>
        </div>

    </xsl:template>

    <xsl:template name="view-open">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="mets:FLocat[@LOCTYPE='URL']/@xlink:href"/>
            </xsl:attribute>
            <i18n:text>xmlui.dri2xhtml.METS-1.0.item-files-viewOpen</i18n:text>
        </a>
    </xsl:template>

    <xsl:template name="display-rights">
        <xsl:variable name="file_id" select="jstring:replaceAll(jstring:replaceAll(string(@ADMID), '_METSRIGHTS', ''), 'rightsMD_', '')"/>
        <xsl:variable name="rights_declaration" select="../../../mets:amdSec/mets:rightsMD[@ID = concat('rightsMD_', $file_id, '_METSRIGHTS')]/mets:mdWrap/mets:xmlData/rights:RightsDeclarationMD"/>
        <xsl:variable name="rights_context" select="$rights_declaration/rights:Context"/>
        <xsl:variable name="users">
            <xsl:for-each select="$rights_declaration/*">
                <xsl:value-of select="rights:UserName"/>
                <xsl:choose>
                    <xsl:when test="rights:UserName/@USERTYPE = 'GROUP'">
                       <xsl:text> (group)</xsl:text>
                    </xsl:when>
                    <xsl:when test="rights:UserName/@USERTYPE = 'INDIVIDUAL'">
                       <xsl:text> (individual)</xsl:text>
                    </xsl:when>
                </xsl:choose>
                <xsl:if test="position() != last()">, </xsl:if>
            </xsl:for-each>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="not ($rights_context/@CONTEXTCLASS = 'GENERAL PUBLIC') and ($rights_context/rights:Permissions/@DISPLAY = 'true')">
                <a href="{mets:FLocat[@LOCTYPE='URL']/@xlink:href}">
                    <img width="64" height="64" src="{concat($theme-path,'/images/Crystal_Clear_action_lock3_64px.png')}" title="Read access available for {$users}"/>
                    <!-- icon source: http://commons.wikimedia.org/wiki/File:Crystal_Clear_action_lock3.png -->
                </a>
            </xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="view-open"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="getFileIcon">
        <xsl:param name="mimetype"/>
            <i aria-hidden="true">
                <xsl:attribute name="class">
                <xsl:text>fa </xsl:text>
                <xsl:choose>
                    <xsl:when test="contains(mets:FLocat[@LOCTYPE='URL']/@xlink:href,'isAllowed=n')">
                        <xsl:text> fa-lock</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <!-- DUL CUSTOMIZATION: don't render file open icon, use .png instead via CSS -->
                    </xsl:otherwise>
                </xsl:choose>
                </xsl:attribute>
            </i>
    </xsl:template>

    <!-- Generate the license information from the file section -->
    <xsl:template match="mets:fileGrp[@USE='CC-LICENSE']" mode="simple">
        <li><a href="{mets:file/mets:FLocat[@xlink:title='license_text']/@xlink:href}"><i18n:text>xmlui.dri2xhtml.structural.link_cc</i18n:text></a></li>
    </xsl:template>

    <!-- Generate the license information from the file section -->
    <xsl:template match="mets:fileGrp[@USE='LICENSE']" mode="simple">
        <li><a href="{mets:file/mets:FLocat[@xlink:title='license.txt']/@xlink:href}"><i18n:text>xmlui.dri2xhtml.structural.link_original_license</i18n:text></a></li>
    </xsl:template>

    <!--
    File Type Mapping template

    This maps format MIME Types to human friendly File Type descriptions.
    Essentially, it looks for a corresponding 'key' in your messages.xml of this
    format: xmlui.dri2xhtml.mimetype.{MIME Type}

    (e.g.) <message key="xmlui.dri2xhtml.mimetype.application/pdf">PDF</message>

    If a key is found, the translated value is displayed as the File Type (e.g. PDF)
    If a key is NOT found, the MIME Type is displayed by default (e.g. application/pdf)
    -->
    <xsl:template name="getFileTypeDesc">
        <xsl:param name="mimetype"/>

        <!--Build full key name for MIME type (format: xmlui.dri2xhtml.mimetype.{MIME type})-->
        <xsl:variable name="mimetype-key">xmlui.dri2xhtml.mimetype.<xsl:value-of select='$mimetype'/></xsl:variable>

        <!--Lookup the MIME Type's key in messages.xml language file.  If not found, just display MIME Type-->
        <i18n:text i18n:key="{$mimetype-key}"><xsl:value-of select="$mimetype"/></i18n:text>
    </xsl:template>

</xsl:stylesheet>
